# dopamine_test

> A VueJS project for interview proccess in Dopamine

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

## Tested with
* Node v8.9.3
* npm v6.3.0
* VueJS v2.5.17
* Vuex v3.0.1
* Chrome v69
* Firefox v62
