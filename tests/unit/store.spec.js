import { getters, mutations } from '@/store'

describe('getters', () => {
  const state = {
    notifications: [{}],
    notificationsCounter: 1,
    loading: false
  }
  it('getNotifications', () => {
    let notifications = getters.getNotifications(state)
    expect(notifications instanceof Array).toBe(true)
    expect(notifications.length).toBe(1)
  })

  it('getNotificationsCounter', () => {
    let notificationsCounter = getters.getNotificationsCounter(state)
    expect(typeof notificationsCounter).toBe('number')
    expect(notificationsCounter).toBe(1)
  })

  it('getLoading', () => {
    let loading = getters.getLoading(state)
    expect(typeof loading).toBe('boolean')
    expect(loading).toBe(false)
  })
})

describe('mutations', () => {
  it('setNotifications', () => {
    const state = {
      notifications: []
    }
    let notificationsToAdd = [{
      id: 1321,
      type: 'text',
      title: 'Test notification',
      text: 'Test text notification',
      expires: 3600
    }]

    expect(state.notifications.length).toBe(0)
    mutations.setNotifications(state, notificationsToAdd)
    expect(state.notifications.length).toBe(1)
    notificationsToAdd.push({
      id: 1321,
      type: 'text',
      title: 'Test notification',
      text: 'Test text notification',
      expires: 3600
    })
    mutations.setNotifications(state, notificationsToAdd)
    expect(state.notifications.length).toBe(2)
  })

  it('setNotificationsCounter', () => {
    const state = {
      notifications: [{
        id: 1321,
        type: 'text',
        title: 'Test notification',
        text: 'Test text notification',
        expires: 3600
      }, {
        id: 4322,
        type: 'bonus',
        title: 'You win a bonus!',
        requirement: 'Deposit $50 to win',
        expires: 3600
      }],
      notificationsCounter: 0
    }
    mutations.setNotificationsCounter(state)
    expect(state.notificationsCounter).toBe(1)
    state.notifications.push({
      id: 4323,
      type: 'text',
      title: 'test text',
      text: 'Test text notification',
      expires: 3600
    })
    mutations.setNotificationsCounter(state)
    expect(state.notificationsCounter).toBe(2)
  })

  it('toggleLoading', () => {
    const state = { loading: false }
    mutations.toggleLoading(state)
    expect(state.loading).toBe(true)
    mutations.toggleLoading(state)
    expect(state.loading).toBe(false)
  })

  it('setActiveMockup', () => {
    const state = { activeMockup: '' }
    expect(state.activeMockup).toBe('')
    mutations.setActiveMockup(state, 'test')
    expect(state.activeMockup).toBe('test')
  })
})
