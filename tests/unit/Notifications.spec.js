import { shallowMount, createLocalVue } from '@vue/test-utils'
import Notifications from '../../src/components/Notifications.vue'
import Vuetify from 'vuetify'

const localVue = createLocalVue()
localVue.use(Vuetify)

const createComponent = propsData => shallowMount(Notifications, { localVue, propsData })

const propsData = {
  notifications: [
    {
      id: 1321,
      type: 'text',
      title: 'Test notification',
      text: 'Test text notification',
      expires: 3600
    }
  ],
  notificationsCounter: 1
}

describe('Notifications.vue', () => {
  let wrapper = null

  beforeEach(() => {
    wrapper = createComponent(propsData)
  })

  it('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBe(true)
  })

  it('is a Notifications component', () => {
    expect(wrapper.is(Notifications)).toBe(true)
  })

  describe('Notifications props', () => {
    it('has notifications prop', () => {
      expect(wrapper.props('notifications')).toBeTruthy()
    })

    it('prop notifications is of type Array and has length 1', () => {
      expect(wrapper.vm.notifications instanceof Array).toBe(true)
      expect(wrapper.vm.notifications.length === 1).toBe(true)
    })

    it('has notificationsCounter prop', () => {
      expect(wrapper.props('notificationsCounter')).toBeTruthy()
    })

    it('prop notificationsCounter is of type number and its value is 1', () => {
      expect(typeof wrapper.vm.notificationsCounter).toBe('number')
      expect(wrapper.vm.notificationsCounter === 1).toBe(true)
    })
  })

  // it('renders notifications count in badge correctly', () => {
  //   expect(wrapper.find('.dop-badge').text()).toBe('1')
  // })

  // it('renders notification title correctly', () => {
  //   expect(wrapper.find('.dop-item-title').text()).toBe('Test notification')
  // })
})
