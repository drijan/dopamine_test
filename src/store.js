import Vue from 'vue'
import Vuex from 'vuex'
import { mockup } from './mocked_data'

Vue.use(Vuex)

const state = {
  notifications: [],
  notificationsCounter: 0,
  loading: false,
  activeMockup: ''
}

const getters = {
  getNotifications: state => state.notifications,
  getNotificationsCounter: state => state.notificationsCounter,
  getLoading: state => state.loading
}

const mutations = {
  setNotifications (state, notifications) {
    state.notifications = notifications
  },
  setNotificationsCounter (state) {
    let relevantNotificationsCount = state.notifications.filter(n => n.type !== 'bonus').length
    state.notificationsCounter = relevantNotificationsCount
  },
  toggleLoading (state) {
    state.loading = !state.loading
  },
  setActiveMockup (state, mockup) {
    state.activeMockup = mockup
  }
}

const actions = {
  // mocked inital data fetch
  loadData ({ commit, dispatch }) {
    commit('toggleLoading')

    setTimeout(() => {
      commit('toggleLoading')
      commit('setNotifications', mockup['initialData'])
      commit('setNotificationsCounter')
      commit('setActiveMockup', 'initalMockup')

      dispatch('setTimeoutOnNotifications')
    }, 400)
  },

  refreshData ({ commit, dispatch }, { refreshedData, currentMockup }) {
    commit('setNotifications', refreshedData)
    commit('setNotificationsCounter')
    commit('setActiveMockup', currentMockup)

    dispatch('setTimeoutOnNotifications')
  },

  // handling notification expiration
  setTimeoutOnNotifications ({ state, commit }) {
    state.notifications.forEach(notification => {
      if (notification.expires) {
        setTimeout(() => {
          let filteredNotifications = state.notifications.filter(n => n.id !== notification.id)
          commit('setNotifications', filteredNotifications)
          commit('setNotificationsCounter')
        }, notification.expires)
      }
    })
  }
}

const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions
})

setInterval(() => {
  let dataToShow, activeMockup
  let allMockupKeys = Object.keys(mockup)
  let indexOfActiveMockup = allMockupKeys.indexOf(store.state.activeMockup)

  if (indexOfActiveMockup < allMockupKeys.length - 1) {
    activeMockup = allMockupKeys[indexOfActiveMockup + 1]
  } else {
    activeMockup = allMockupKeys[0]
  }

  dataToShow = mockup[activeMockup]
  store.dispatch('refreshData', { refreshedData: dataToShow, currentMockup: activeMockup })
}, 10000)

export { store, getters, mutations, actions }
