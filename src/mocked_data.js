const initialData = [
  {
    id: 1321,
    type: 'text',
    title: 'Test notification',
    text: 'Test text notification',
    expires: 3600
  },
  {
    id: 4322,
    type: 'bonus',
    title: 'You win a bonus!',
    requirement: 'Deposit $50 to win',
    expires: 3600
  },
  {
    id: 5453,
    type: 'Promotion',
    image: 'http://www.absolat.com/images/promotion-in-marketing.jpg',
    title: '%30 off on sports betting',
    link: 'https://www.google.com/'
  },
  {
    id: 5236,
    type: 'text',
    title: 'Test notification',
    text: 'Test text notification',
    expires: 5000
  }
]

const changedData1 = [
  {
    id: 1321,
    type: 'text',
    title: 'Test notification',
    text: 'Test text notification',
    expires: 3600
  },
  {
    id: 2020,
    type: 'bonus',
    title: 'You win a bonus!',
    requirement: 'Deposit $10 to win',
    expires: 6600
  },
  {
    id: 4322,
    type: 'bonus',
    title: 'You win a bonus!',
    requirement: 'Deposit $30 to win',
    expires: 3600
  },
  {
    id: 5453,
    type: 'Promotion',
    image: 'http://www.absolat.com/images/promotion-in-marketing.jpg',
    title: '%30 off on horse racing betting',
    link: 'https://www.google.com/'
  },
  {
    id: 5236,
    type: 'text',
    title: 'Test notification',
    text: 'Test text notification',
    expires: 5000
  }
]

const changedData2 = [
  {
    id: 2020,
    type: 'bonus',
    title: 'You win a bonus!',
    requirement: 'Deposit $10 to win',
    expires: 6600
  },
  {
    id: 5453,
    type: 'Promotion',
    image: 'http://www.absolat.com/images/promotion-in-marketing.jpg',
    title: '%30 off on horse racing betting',
    link: 'https://www.google.com/'
  },
  {
    id: 5236,
    type: 'text',
    title: 'Test 1 notification',
    text: 'Test 1 text notification',
    expires: 5000
  },
  {
    id: 5236,
    type: 'text',
    title: 'Test 2 notification',
    text: 'Test 2 text notification',
    expires: 7000
  }
]

export const mockup = {
  initialData,
  changedData1,
  changedData2
}
