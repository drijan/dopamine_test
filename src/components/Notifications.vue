<template>
  <div class="text-xs-center">
      <v-btn class="dop-btn" @click="toggleNotificationsList">
        <v-badge color="red" class="dop-badge">
          <span slot="badge">{{ notificationsCounter }}</span>
          <v-icon
            large
            color="grey"
          >
            notifications
          </v-icon>
        </v-badge>


        <transition name="dop-list-slide">
          <div v-if="showNotificationsList" class="dop-list-wrapper">
            <div class="dop-list-title">NOTIFICATIONS</div>

            <transition-group name="slide-fade" mode="in-out">
              <div
                v-for="(item, index) in notifications"
                :key="item.id"
              >
                <v-list-tile class="dop-list-item">
                  <v-list-tile-avatar>
                    <img v-if="item.image" :src="item.image" :key="item.image">
                    <span v-else>
                      <v-icon v-if="item.type === 'bonus'" color="red">card_giftcard</v-icon>
                      <v-icon v-if="item.type === 'text'" color="primary">info</v-icon>
                    </span>
                  </v-list-tile-avatar>

                  <v-list-tile-content>
                    <v-list-tile-title
                      class="dop-item-title"
                      :key="item.title"
                    >
                      <a v-if="item.link" :href="item.link" target="_blank">{{ item.title }}</a>
                      <span v-else>{{ item.title }}</span>
                    </v-list-tile-title>

                    <v-list-tile-sub-title
                      v-show="item.text"
                      :key="item.text"
                      class="dop-item-text"
                    >
                      {{ item.text }}
                    </v-list-tile-sub-title>

                    <v-list-tile-sub-title
                      v-if="item.requirement"
                      :key="item.requirement"
                      class="dop-item-text"
                    >
                      {{ item.requirement }}
                    </v-list-tile-sub-title>
                  </v-list-tile-content>
                </v-list-tile>

                <v-divider
                  class="dop-list-divider"
                  v-if="index + 1 < notifications.length"
                  :key="`divider-${index}`"
                ></v-divider>
              </div>
            </transition-group>
          </div>
        </transition>
      </v-btn>
  </div>
</template>

<script>
export default {
  name: 'Notifications',
  props: {
    notifications: {
      type: Array,
      required: true
    },
    notificationsCounter: {
      type: Number,
      required: true
    }
  },
  data () {
    return {
      showNotificationsList: false
    }
  },
  methods: {
    toggleNotificationsList () {
      this.showNotificationsList = !this.showNotificationsList
    }
  }
}
</script>

<style scoped>
  .dop-btn {
    height: 50px;
    min-width: 50px;
    width: 50px;
    background-color: white !important;
    border-radius: 5px;
  }

  .dop-list-wrapper {
    width: 350px;
    position: absolute;
    top: 60px;
    right: -25px;
    z-index: 3;
    box-shadow: 0 5px 5px -3px rgba(0,0,0,.2), 0 8px 10px 1px rgba(0,0,0,.14), 0 3px 14px 2px rgba(0,0,0,.12);
    border-radius: 5px;
  }

  .dop-list-wrapper::after {
    bottom: 100%;
    border: solid transparent;
    content: " ";
    height: 0;
    width: 0;
    position: absolute;
    right: 20px;
    top: -40px;
    pointer-events: none;
    border-bottom-color: #6a98ee;
    border-width: 30px;
    margin-left: -30px;
  }

  .dop-list-title {
    width: 100%;
    height: 40px;
    background-color: #6a98ee;
    color: white;
    text-align: start;
    font-weight: bold;
    padding-left: 10px;
    padding-top: 10px;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
  }

  .dop-list-item {
    padding: 15px 0;
  }

  .dop-list-divider {
    margin: 0 30px;
  }

  .dop-item-title {
    font-weight: bold;
    text-transform: none;
  }

  .dop-item-text {
    text-transform: none;
  }

  /** animation style for notificaitons list  **/
  .dop-list-slide-enter-active, .dop-list-slide-leave-active {
    transition: all .3s;
  }
  .dop-list-slide-enter, .dop-list-slide-leave-to {
    opacity: 0;
    transform: translateY(30px);
  }

  /** animation style for notificaitons list items  **/
  .slide-fade-enter-active {
    transition: all .3s ease;
  }
  .slide-fade-leave-active {
    transition: all .5s cubic-bezier(1.0, 0.5, 0.8, 1.0);
  }
  .slide-fade-enter, .slide-fade-leave-to {
    transform: translateX(50px);
    opacity: 0;
  }
</style>
